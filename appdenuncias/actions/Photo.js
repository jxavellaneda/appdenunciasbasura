import constants from '../constants/Photo';

const addPhoto = photo => {
  return {
    type: constants.PHOTO_ADD,
    photo
  }
};

export default {
  addPhoto
}
