import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import ReportTypeSelectScreen from '../screens/ReportTypeSelectScreen';
import ReportCameraScreen from '../screens/ReportCameraScreen';
import ReportPhotoScreen from '../screens/ReportPhotoScreen';
import ReportFormScreen from '../screens/ReportFormScreen';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

const ReportTypeSelectStack = createStackNavigator({
  ReportTypeSelect: ReportTypeSelectScreen,
  ReportCamera: ReportCameraScreen,
  ReportPhoto: ReportPhotoScreen,
  ReportForm: ReportFormScreen
});

ReportTypeSelectStack.navigationOptions = {
  tabBarLabel: 'Denunciar',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-camera' : 'md-camera'}
    />
  ),
};

const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
});

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
    />
  ),
};

export default createBottomTabNavigator({
  HomeStack,
  ReportTypeSelectStack,
  SettingsStack,
});
