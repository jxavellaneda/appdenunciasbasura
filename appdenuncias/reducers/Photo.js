import constants from '../constants/Photo';

const INITIAL_STATE = [];

export default photoReducer = (state = INITIAL_STATE, {type, photo}) => {
  switch (type) {
    case constants.PHOTO_ADD:
      return [
        ...state,
        photo
      ]
    default:
      return state
  }
};