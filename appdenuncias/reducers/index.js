import { combineReducers } from 'redux';

import photoReducer from './Photo';

export default combineReducers({
  photos: photoReducer
});