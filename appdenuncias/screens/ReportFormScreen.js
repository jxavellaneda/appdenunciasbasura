import React from 'react';
import { connect } from 'react-redux';

import { StyleSheet, Text, ScrollView, Image, Button, Dimensions, TouchableOpacity, TextInput } from 'react-native';
import { Camera, Permissions, Location, MapView } from 'expo';


import PhotoActions from '../actions/Photo';

class ReportFormScreen extends React.Component {

  state = {
    location: null,
    errorMessage: null,
    description: ''
  };


  static navigationOptions = {
    title: 'Photo Screen',
  };

  // componentWillMount() {
  //   if (Platform.OS === 'android' && !Constants.isDevice) {
  //     this.setState({
  //       errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
  //     });
  //   } else {
  //     this._getLocationAsync();
  //   }
  // }

  async _getLocationAsync() {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    this.setState({ location });
    console.log(location);
  };
  
  render() {
    const { navigation, addPhoto, cachedPhotos } = this.props;
    const photo = navigation.getParam('photo', { uri: '' });
    const { height, width } = Dimensions.get('window');
    const { description } = this.state;
    
    return (
      <ScrollView style={{ flex: 1 }}>
        <Text>
          Descripción:
        </Text>
        <TextInput
          multiline={true}
          style={{height: 100, borderColor: 'gray', borderWidth: 1}}
          onChangeText={text => this.setState({ description: text })}
          value={description}/>
        
        <Text>
          Localización:
        </Text>
        <MapView
          style={{ flex: 1, height: 350, width: 370 }}
          initialRegion={{
            latitude: -37.31504700615752,
            longitude: -59.12571740342222,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}>
             <MapView.Marker
              draggable
              coordinate={{
                latitude: -37.31504700615752,
                longitude: -59.12571740342222}}
              title={"marker.title"}
              description={"desss"}/>
        </MapView>
        <Button
          style={{
            flex: 1,
            backgroundColor: 'transparent',
            flexDirection: 'row',
          }}
          onPress={() => this._getLocationAsync()}
          title="Usar ubicación actual"/>
        <Button
          style={{
            flex: 1,
            backgroundColor: 'transparent',
            flexDirection: 'row',
          }}
          onPress={() => alert('navigate')}
          title="Confirmar"/>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});

const mapStateToProps = ({ photos }) => {
  return {
    cachedPhotos: photos
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addPhoto: photo => dispatch(PhotoActions.addPhoto(photo))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReportFormScreen);
// export default ReportFormScreen;
