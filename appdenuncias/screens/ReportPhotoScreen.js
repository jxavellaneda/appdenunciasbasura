import React from 'react';
import { connect } from 'react-redux';

import { StyleSheet, Text, View, Image, Button, Dimensions, TouchableOpacity } from 'react-native';
import { Camera, Permissions } from 'expo';

import PhotoActions from '../actions/Photo';

class ReportTypeSelectScreen extends React.Component {

  static navigationOptions = {
    title: 'Photo Screen',
  };
  
  render() {
    const { navigation, addPhoto, cachedPhotos } = this.props;
    const photo = navigation.getParam('photo', { uri: '' });
    const { height, width } = Dimensions.get('window');
    
    console.log(cachedPhotos);
    return (
      <View style={{ flex: 1 }}>
        <Button
          style={{
            flex: 1,
            backgroundColor: 'transparent',
            flexDirection: 'row',
          }}
          onPress={() => navigation.navigate('ReportForm')}
          title="Confirmar"/>
         <Button
          style={{
            flex: 1,
            backgroundColor: 'transparent',
            flexDirection: 'row',
          }}
          onPress={() => {
            addPhoto(photo);
            navigation.navigate('ReportCamera');
          }}
          title="+"/>
        <Image
          style={{width, height}}
          source={{uri: photo.uri}}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});

const mapStateToProps = ({ photos }) => {
  return {
    cachedPhotos: photos
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addPhoto: photo => dispatch(PhotoActions.addPhoto(photo))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReportTypeSelectScreen);
// export default ReportTypeSelectScreen;
