import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { Camera, Permissions } from 'expo';

export default class ReportTypeSelectScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = {
    title: 'Report',
  };
  
  render() {
    const { navigate } = this.props.navigation;
 
    return (
      <View style={{ flex: 1 }}>
        <Button
          onPress={() => navigate('ReportCamera')}
          title="Tomar Foto"
        />
        <Button
          onPress={() => alert('navigate!')}
          title="Subir Foto"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
